import scala.collection.immutable.List

object Cw2 extends App {
  val dayList = List("poniedzialek", "wtorek", "sroda", "czwartek", "piatek", "sobota", "niedziela")
  println(cw2a(0))
  println(cw2b(dayList.size - 1))

  def cw2a(currntItem: Int): String = {
    var result: String = ""
    result += (dayList(currntItem))
    if (currntItem < dayList.size - 1) {
      result += ","
      result += (cw2a(currntItem + 1))
    }
    return result
  }

  def cw2b(currntItem: Int): String = {
    var result: String = ""
    result += (dayList(currntItem))
    if (currntItem > 0) {
      result += ","
      result += cw2b(currntItem - 1)
    }
    return result
  }
}
