import scala.collection.immutable._

object Cw1 extends App {
 val dayList = List("poniedzialek", "wtorek", "sroda", "czwartek", "piatek", "sobota", "niedziela")
  cw1a()
  cw1b()
  cw1c()

  def cw1a(): Unit = {
    println("---1.a---------------------------------------------------------------------------------")
    for (day <- dayList) {
      println(day)
    }
    println()

  }

  def cw1c(): Unit = {
    println("---1.c--------------------------------------------------------------------------------")
    var counter: Int = 0
    while (counter < dayList.size) {
      println(dayList(counter))
      counter += 1
    }
  }

  def cw1b(): Unit = {
    println("---1.b---------------------------------------------------------------------------------")
    for (day <- dayList) {
      if (day.startsWith("p")) {
        println(day)
      }
    }
    println()
  }
}
