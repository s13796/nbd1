object Cw7 extends App {
  val capitals = Map("Jajka" -> 22.3, "Mleko" -> 2.3)

  println("Jajka " + show(capitals.get( "Jajka")) )
  println("Mleko " + show(capitals.get( "Mleko")) )

  def show(x: Option[Double]) = x match {
    case Some(s) => "Kosztuje " + s + "zł"
    case None => "Nie kupuje"
  }
}
