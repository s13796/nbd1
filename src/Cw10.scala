import scala.collection.immutable.List

object Cw10 extends App {
  val litaLczb = List(1.0, 2.5, 0.4, 7.4, 9.4, -0.5, -6)

  println(cw10(litaLczb))

  def cw10(listaWejsciowa: List[Double]): List[Double] = {
    val lista = listaWejsciowa.filter(_ >= -5).filter(_ <= 12)
    val listaLiczb = lista map {
      case (value) => {
        if (value >= 0) {
          value
        } else {
          0 - value
        }
      }
    }
    listaLiczb
  }
}
