import scala.collection.immutable.List

object Cw8 extends App {
  val lista = List(1, 2, 0, 7, 9, 0, 6)

  for (element <- cw8(lista, 0, List[Int]())) {
    println(element)
  }

  def cw8(listaWejsciowa: List[Int], counter: Int, listaWyjsciowa: List[Int]): List[Int] = {
    var lista = listaWyjsciowa;
    if (counter < listaWejsciowa.size) {
      if (listaWejsciowa(counter) != 0) {
        lista = listaWejsciowa(counter) :: listaWyjsciowa
      }
      return cw8(listaWejsciowa, counter + 1, lista)
    }
    listaWyjsciowa
  }


}
