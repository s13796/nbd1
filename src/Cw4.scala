import scala.collection.immutable.List

object Cw4 extends App {
  val dayList = List("poniedzialek", "wtorek", "sroda", "czwartek", "piatek", "sobota", "niedziela")
  var result: String = ""


  println("Cw4a: " + (
    dayList.foldLeft("")((a, b) => {
      if (a.equals("")) {
        b
      } else {
        a + "," + b
      }
    }))
  )

  println("Cw4b: " + (
    dayList.foldRight("")((a, b) => {
      if (b.equals("")) {
        a
      } else {
        a + "," + b
      }
    }))
  )

  println("Cw4c: " + (
    dayList.foldLeft("")((a, b) => {
      if (a.startsWith("p") && b.startsWith("p")) {
        a + "," + b
      } else if (a.startsWith("p")) {
        a
      } else if (b.startsWith("p")) {
        b
      } else {
        ""
      }
    }))
  )
}