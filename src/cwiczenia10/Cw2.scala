package cwiczenia10

object Cw2 extends App {
  def doulesFromInt(a: Int): Double = a.toDouble
  def yesFromInt(a: Int): Maybe[Int] = Yes(a)

  val no = No()
  val yes = Yes(0)
  println(no.map(doulesFromInt))
  println(yes.map(doulesFromInt))
  println(yes.flatMap(yesFromInt))
}


trait Maybe[+A] {

  def map[R](f: A => R): Maybe[R]
  def flatMap[R](f: A => Maybe[R]): Maybe[R]
}

case class No() extends Maybe[Nothing] {

  override def map[R](f: Nothing => R): Maybe[R] = No()

  override def flatMap[R](f: Nothing => Maybe[R]): Maybe[R] = No()
}

case class Yes[A](val value: A) extends Maybe[A] {

  override def map[R](f: A => R): Maybe[R] = Yes(f(value))

  override def flatMap[R](f: A => Maybe[R]): Maybe[R] = f(value)
}