package cwiczenia10

object Cw1 extends App {

  def pairIterator = for {
    a <- Iterator.from(1)
    b <- 1 until a + 1 if a % b == 0
  } yield (a, b)

  println("przy użyciu take: ")
  pairIterator.take(20).foreach(print)

  println

  println("przy użyciu next: ")
  var pairIteratorBuffer = pairIterator.buffered
  for (i <- 1 to 20) {
    print(pairIteratorBuffer.next())
  }
}
