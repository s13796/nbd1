!pip install riak
import riak

client = riak.RiakClient(pb_port = 8098)
mojMucket = client.bucket('s13796')

dokumentDoBazy = mojMucket.new('os1', data = {'imie': 'Jan', 'nazwisko': 'Kowalski', 'dataUr': '08 Luty 1996'})
dokumentDoBazy.store()
print('Pierwsza osoba w bazie to: ' + str(mojMucket.get('os1').data))

dokumentZBazy = mojMucket.get('os1')
dokumentZBazy.data['nazwisko'] = 'Nowe'
dokumentZBazy.store()
print('Pierwsza osoba w bazie z nowum nazwiskiemja  to: ' + str(mojMucket.get('os1').data))

dokumentZBazy.delete()
print('Pierwsza osoba w bazie z nowum nazwiskiemja  to ' + str(mojMucket.get('os1').data))