import scala.annotation.tailrec
import scala.collection.immutable.List

object Cw3 extends App {
  val dayList = List("poniedzialek", "wtorek", "sroda", "czwartek", "piatek", "sobota", "niedziela")
  var result: String = ""
  cw3(0)
  println(result)

  @tailrec
  def cw3(currntItem: Int): Unit = {
    result += (dayList(currntItem))
    if (currntItem < dayList.size - 1) {
      result += ","
      cw3(currntItem + 1)
    }
  }
}
