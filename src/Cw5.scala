object Cw5 extends App {
  val nazwyICenyProduktowMap = Map("ser" -> 10.0, "mleko" -> 12.0, "jajka" -> 30.0)

  val newMap = nazwyICenyProduktowMap map {
    case (key, value) => (key, value*0.1)
  }
  println(newMap)
}
