package cwiczenia2

object Cw3 extends App {

  class Osoba(val imie: String, val nazwisko: String)

  println(przywitanie(new Osoba("a", "aa")))
  println(przywitanie(new Osoba("b", "bb")))
  println(przywitanie(new Osoba("c", "cc")))
  println(przywitanie(new Osoba("d", "dd")))

  def przywitanie(osoba: Osoba): String = osoba.nazwisko match {
    case "aa" => "Witaj a"
    case "bb" => "Witaj b"
    case "cc" => "Witaj c"
    case _ => "Witam Cie o nieznajomy"
  }
}
