package cwiczenia2

object Cw4 extends App {

  infunkcja(1, funkcja)

  def funkcja = (wartosc: Int) => {
    println(wartosc)
  }

  def infunkcja(wartosc: Int, funkcja: (Int) => Unit) = {
    funkcja(wartosc)
  }
}
