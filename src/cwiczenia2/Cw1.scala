package cwiczenia2

object Cw1 extends App {
  println(czyPraca("poniedziałek"))
  println(czyPraca("wtorek"))
  println(czyPraca("środa"))
  println(czyPraca("czwartek"))
  println(czyPraca("piątek"))
  println(czyPraca("sobota"))
  println(czyPraca("niedziela"))
  println(czyPraca("nie ma takiego"))

  def czyPraca(x: String): String = x match {
    case "poniedziałek" | "wtorek" | "środa" | "czwartek" | "piątek" => "praca"
    case "sobota" | "niedziela" => "weekend"
    case _ => "Nie ma takiego dnia"
  }
}
