package cwiczenia2

object Cw5 extends App {

  var student = new Student{}
  var nauczyciel = new Nauczyciel{}
  var pracownik = new Pracownik{}

  pracownik.pensja = 5000
  nauczyciel.pensja = 2000

  println("Pensja nauczyciela: " + nauczyciel.pensja)
  println("Podatek nauczyciela: " + nauczyciel.podatek)

  println("Pensja pracownika: " + pracownik.pensja)
  println("Podatek pracownika: " + pracownik.podatek)

  println("Podatek student: " + student.podatek)


  trait Student extends Osoba {
    override def podatek: Double = 0.0
  }

  trait Nauczyciel extends Pracownik {
    override def podatek: Double = 0.1 * pensja
  }

  trait Pracownik extends Osoba {
    private var _pensja : Double = 0
    def pensja = _pensja

    override def podatek: Double = 0.2 * pensja
    def pensja_= (kwota : Double) = _pensja = (kwota - podatek)
  }



  class Osoba {
    private val _imie: String = ""
    private val _nazwisko: String = ""
    private var _podatek: Double = 1.0

    def imie = _imie
    def nazwisko = _nazwisko
    def podatek = _podatek
  }
}
