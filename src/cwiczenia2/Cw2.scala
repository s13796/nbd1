package cwiczenia2

object Cw2 extends App {


  class KontoBankowe(val _stanKonta: Double) {

    private var stanKonta = _stanKonta

    def this() {
      this(0)
    }

    def wplata(num: Int) = {
      stanKonta += num
    }

    def wyplata(num: Int) = {
      stanKonta -= num
    }
  }
}
