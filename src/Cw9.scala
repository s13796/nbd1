import scala.collection.immutable.List

object Cw9 extends App {
  val litaLczb = List(1, 2, 0, 7, 9, 0, 6)


  println(cw9(litaLczb))

  def cw9(listaWejsciowa: List[Int]): List[Int] = {
    val listaLiczb = litaLczb map {
      case (value) => (value + 1)
    }
    listaLiczb
  }
}
