package cwiczenia9

import scala.collection.immutable._

object Cw1 extends App {
  println("---1---------------------------------------------------------------------------------")
  val container = new Container[String]("Ala")
  println("getContent: "+container.getContent)
  println("getContent: "+container.applyFunction((s: String) => s.length))


  println("---2---------------------------------------------------------------------------------")
  val yes = new Yes[String]("Ala")
  val no = new No

  println(yes.isInstanceOf[Maybe[_]])
  println(no.isInstanceOf[Maybe[_]])

  println("---3---------------------------------------------------------------------------------")
  println(no.applyFunction[String](_ => "x").isInstanceOf[No])
  println(yes.applyFunction[String](_ => "s").isInstanceOf[Yes[_]])

  println("---4---------------------------------------------------------------------------------")
  println(no.getOrElse("S"))
  println(yes.getOrElse("S"))

}


class Container[A](private val value: A) {

  def getContent: A = value

  def applyFunction[R](function: A => R): R = function(value)
}

trait Maybe[+A] {
  def applyFunction[R](fun: A => R): Maybe[R]

  def getOrElse[A](a: A)
}

class No extends Maybe[Nothing] {

  override def applyFunction[R](fun: Nothing => R): No = new No()

  override def getOrElse[A](a: A): Unit = a
}

class Yes[A](val value: A) extends Maybe[A] {

  def applyFunction[R](f: A => R): Yes[R] = {
    new Yes(f(value))
  }

  override def getOrElse[A](a: A): Unit = new Yes[String]("Ala")
}